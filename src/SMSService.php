<?php

namespace ITPolice\SmsServices;

interface SMSService {
    public function __construct($phone);
    public function setPhone(string $phone);
    public function setEmail(string $email);
    public function setSender(string $sender);
    public function send($msg, $type = '', $saveCode = false);
    public function sendEmail($msg, $subject = '', $senderEmail = '', $senderName = ''): bool;
    public function checkCode($code, $type);
    public function deleteCodes($type);
    public function getBlockSmsSeconds($type = '', $max = null);
    public function hasCodeInSession($type = '');
    public function generateRandomPassword($length = 6);
    public function sendInApi($msg);
    public function canUseVoiceCall(): bool;
    public function canUseEmailMessage(): bool;
    public function canUseFlashCall(): bool;
    public function prepareVoiceMenu(): string;    
    public function isActive(): bool;
}
