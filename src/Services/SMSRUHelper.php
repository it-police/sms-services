<?php

namespace ITPolice\SmsServices\Services;

use Illuminate\Support\Facades\Log;

class SMSRUHelper extends SMSService implements \ITPolice\SmsServices\SMSService
{
    public function sendInApi($msg) {
        $data = [
            'api_id'   => env('SMSRU_API_ID'),
            'to'       => $this->phone,
            'msg'      => $msg,
            'translit' => env('SMSRU_TRANSLIT', true) ? 1 : 0,
            'json'     => 1
        ];
        
        if (env('SMSRU_FROM', false)){
            $data['from'] = env('SMSRU_FROM');
        }

        if ( ! empty($this->sender)) {
            $data['from'] = $this->sender;
        }
        
        $url = "https://sms.ru/sms/send?" . http_build_query($data);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $body = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($body);
        if (env('SMSRU_LOG_REQUESTS')) {
            Log::debug(__CLASS__, ['data' => $data, 'response' => $body]);
        }

        if (@$json->status === 'OK' && @$json->sms->{$this->phone}->status === 'OK') {
            return true;
        }

        return false;
    }

    public function isActive(): bool
    {
        return ! empty(env('SMSRU_API_ID'));
    }
}
