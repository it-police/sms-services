<?php

namespace ITPolice\SmsServices\Services;

use Illuminate\Support\Facades\Log;
use Laravie\Parser\Xml\Reader;
use Laravie\Parser\Xml\Document;

class BeelineHelper extends SMSService implements \ITPolice\SmsServices\SMSService
{
    protected $addPlus = true;
    
    public function sendInApi($msg) {
        $data = [
            'action'  => 'post_sms',
            'gzip'    => 'none',
            'user'    => env('BEELINE_LOGIN'),
            'pass'    => env('BEELINE_PASSWORD'),
            'target'  => $this->phone,
            'message' => $msg,
            'sender' => env('BEELINE_SENDER')
        ];

        if ( ! empty($this->sender)) {
            $data['sender'] = $this->sender;
        }
        
        $url = "https://a2p-sms-https.beeline.ru/proto/http?" . http_build_query($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencoded; charset=UTF-8'
        ]);
        $body = curl_exec($ch);
        curl_close($ch);
        $xml = (new Reader(new Document()))->extract($body);
        $res = $xml->parse([
            'id' => ['uses' => 'result.sms::id'],
        ]);

        if (env('BEELINE_LOG_REQUESTS')) {
            Log::debug(__CLASS__, [
                'data'     => $data,
                'response' => $body
            ]);
        }

        if ( ! empty($res['id'])) {
            return true;
        }

        return false;
    }

    public function isActive(): bool
    {
        return ! empty(env('BEELINE_LOGIN')) && ! empty(env('BEELINE_PASSWORD'));
    }
}
