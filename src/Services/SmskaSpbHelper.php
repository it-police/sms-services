<?php

namespace ITPolice\SmsServices\Services;

use http\Env;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;


class SmskaSpbHelper extends SMSService implements \ITPolice\SmsServices\SMSService
{
    private $token = null;


    private function getToken(){

        if (Cache::has('SMSKA_SPB_TOKEN') && Cache::get('SMSKA_SPB_TOKEN')) {
            $token = Cache::get('SMSKA_SPB_TOKEN');
        } else {
            $data = [
                'username'   => env('SMSKA_SPB_LOGIN'),
                'password' => env('SMSKA_SPB_PASSWORD')
            ];
            $header[] = 'Content-type: application/json';
            $url = "https://lid.smska-spb.ru/api/login";
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
            curl_setopt($ch,CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            $body = curl_exec($ch);
            curl_close($ch);
            $json = json_decode($body);
            if (env('SMSKA_SPB_LOG_REQUESTS')) {
                Log::debug(__METHOD__, ['data' => $data, 'response' => $body]);
            }
            if (@$json->token){
                $token = $json->token;
                Cache::put('SMSKA_SPB_TOKEN', $token, now()->addHours(24));
            } else {
                throw new \Exception('Не удалось получить токен', 1000);
            }
        }
        $this->token = $token;
    }

    public function sendInApi($msg) {
        try {
            $this->getToken();
        } catch (\Exception $exception){
            Log::error(__CLASS__.' Error', [$exception->getMessage(), $exception->getTraceAsString()]);
            return false;
        }

        $data = [
            'type'   => 'sms',
            'recipient' => $this->phone,
            'payload'      => [
                'sender' => env('SMSKA_SPB_SENDER'),
                'text' => $msg
            ],
        ];
        $header[] = 'Content-type: application/json';
        $header[] = "Authorization: $this->token";

        $url = "https://lid.smska-spb.ru/api/sendings";
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $body = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($body);
        if (env('SMSKA_SPB_LOG_REQUESTS')) {
            Log::debug(__CLASS__, ['data' => $data, 'response' => $body]);
        }

        if (@$json->status !== 'rejected' && @$json->error === null) {
            return true;
        }

        return false;
    }

    public function isActive(): bool
    {
        return ! empty(env('SMSKA_SPB_LOGIN')) && ! empty(env('SMSKA_SPB_PASSWORD'));
    }
}
