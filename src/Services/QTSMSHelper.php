<?php

namespace ITPolice\SmsServices\Services;

use http\Env;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;


class QTSMSHelper extends SMSService implements \ITPolice\SmsServices\SMSService
{
    public function sendInApi($msg) {

        $url = 'https://go.qtelecom.ru/public/http/';

        $data = [
            'user' => env("QTSMS_USER"),
            'pass' => env("QTSMS_PASSWORD"),
            'action' => 'post_sms',
            'message' => $msg,
            'target' => $this->phone,
            'autotrimtext' => 'on',
        ];
        if (env("QTSMS_SENDER", false)) $data['sender'] = env("QTSMS_SENDER", '');
        $url = "$url?" . http_build_query($data);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
        ));
        curl_setopt($ch, CURLOPT_ENCODING , "gzip");
        $body = curl_exec($ch);
        curl_close($ch);
//        $json = json_decode($body);
        if (env('SMS_LOG_REQUESTS')) {
            Log::debug(__CLASS__, ['data' => $data, 'response' => $body]);
        }
        $xml = simplexml_load_string($body);
        if ($xml->result->sms['id'] ?? false){
            return true;
        }
        return false;
    }

    public function isActive(): bool
    {
        return ! empty(env("QTSMS_USER")) && ! empty(env("QTSMS_PASSWORD"));
    }
}



