<?php

namespace ITPolice\SmsServices\Services;

use Illuminate\Support\Facades\Log;

class StreamTelecomHelper extends SMSService implements \ITPolice\SmsServices\SMSService
{
    public function sendInApi($msg)
    {
        $data = [
            'user' => env('STREAM_TELECOM_LOGIN'),
            'pwd' => env('STREAM_TELECOM_PASSWORD'),
            'sadr' => env('STREAM_TELECOM_SENDER'),
            'text' => $msg,
            'dadr' => $this->phone
        ];

        $url = "http://gateway.api.sc/get/?" . http_build_query($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $body = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $json = json_decode($body);
        if (env('STREAM_TELECOM_LOG_REQUESTS')) {
            Log::debug(__CLASS__, [
                'data' => $data,
                'response' => $body
            ]);
        }

        if ($httpCode == 200) {
            return true;
        }

        return false;
    }

    public function canUseFlashCall(): bool
    {
        return true;
    }

    public function flashCallByApi($saveCode = false)
    {
        $data = [
            'login' => env('STREAM_TELECOM_LOGIN'),
            'pass' => env('STREAM_TELECOM_PASSWORD'),
            'type' => 'flash',
            'code' => $saveCode,
            'phone' => $this->phone
        ];

        if (env('STREAM_TELECOM_CALLBACK_URL')) {
            $data['callback_url'] = env('STREAM_TELECOM_CALLBACK_URL');
        }

        $url = "http://gateway.api.sc/flash/?" . http_build_query($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $body = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $json = json_decode($body);
        if (env('STREAM_TELECOM_LOG_REQUESTS')) {
            Log::debug(__CLASS__, [
                'data' => $data,
                'response' => $body
            ]);
        }

        if ($httpCode == 200) {
            return true;
        }

        return false;
    }

    public function voiceCallByApi($msg)
    {
        $data = [
            'login' => env('STREAM_TELECOM_LOGIN'),
            'pass'  => env('STREAM_TELECOM_PASSWORD'),
            'type'  => 'voice',
            'code'  => $msg,
            'phone' => $this->phone
        ];

        if (env('STREAM_TELECOM_CALLBACK_URL')) {
            $data['callback_url'] = env('STREAM_TELECOM_CALLBACK_URL');
        }

        $url = "http://gateway.api.sc/flash/?".http_build_query($data);
        $ch  = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $body     = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $json = json_decode($body);
        if (env('STREAM_TELECOM_LOG_REQUESTS')) {
            Log::debug(__CLASS__, [
                'data'     => $data,
                'response' => $body,
                'code'     => $httpCode
            ]);
        }

        if ($httpCode == 200) {
            return true;
        }

        return false;
    }

    public function canUseVoiceCall(): bool
    {
        return true;
    }

    public function isActive(): bool
    {
        return ! empty(env('STREAM_TELECOM_LOGIN')) && ! empty(env('STREAM_TELECOM_PASSWORD'));
    }
}
