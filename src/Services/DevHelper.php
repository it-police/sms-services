<?php

namespace ITPolice\SmsServices\Services;

use Illuminate\Support\Facades\Log;

class DevHelper extends SMSService implements \ITPolice\SmsServices\SMSService
{

    public function sendInApi($msg)
    {
        $data = [
            'phone'  => $this->phone,
            'message' => $msg
        ];
        Log::debug(__CLASS__, $data);
        return true;
    }

    public function generateRandomPassword($length = 6)
    {
        return str_repeat(2, $length);
    }

    public function isActive(): bool
    {
        return true;
    }
}
