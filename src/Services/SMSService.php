<?php

namespace ITPolice\SmsServices\Services;

use Backpack\Settings\app\Models\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use ITPolice\SmsServices\Parts\VoiceMenu;

abstract class SMSService {

    public $phone;
    public $email;
    public $sender = null;
    protected $addPlus = false;
    /**
     * @var VoiceMenu
     */
    protected $voiceMenu;

    public function __construct($phone = null)
    {
        if($phone) {
            $this->phone = $this->preparePhoneList($phone);
        }
    }

    public function setPhone(string $phone)
    {
        $this->phone = $this->preparePhoneList($phone);
        return $this;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
        return $this;
    }

    public function setSender(string $sender)
    {
        $this->sender = $sender;
        return $this;
    }

    public function send($msg, $type = '', $saveCode = false) {
        if (!env('SMS_REAL_SEND')) {
            $send = true;
        } else {
            $result = $this->sendInApi($msg);
            /** Если пришел массив - значит там должен быть статус и код ошибки */
            if (is_array($result)) {
                $send = $result['status'] ?? false;
                session()->flash('sms_error_code', $result['error_code'] ?? 10);
            } else {
                $send = $result;
            }
        }

        if($send) {
            if($saveCode) {
                DB::table('sms_codes')->insert([
                    'phone'      => $this->phone,
                    'code'       => Hash::make($saveCode),
                    'type'       => $type,
                    'created_at' => Carbon::now(),
                ]);
                //session([$type => $saveCode]);
            }
        }

        return $send;
    }

    public function flashCall($saveCode, $type = '')
    {
        if ( ! env('SMS_REAL_SEND')) {
            $send = true;
        } else {
            $send = $this->flashCallByApi($saveCode);
        }

        if ($send) {
            DB::table('sms_codes')->insert([
                'phone'      => $this->phone,
                'code'       => Hash::make($saveCode),
                'type'       => $type,
                'created_at' => Carbon::now(),
            ]);
        }

        return $send;
    }

    public function authCall($type = '')
    {
        if ( ! env('SMS_REAL_SEND')) {
            $code = $this->generateRandomPassword();
        } else {
            $code = $this->authCallByApi();
        }
        
        if ($code) {
            DB::table('sms_codes')->insert([
                'phone'      => $this->phone,
                'code'       => Hash::make($code),
                'type'       => $type,
                'created_at' => Carbon::now(),
            ]);
        }

        return $code;
    }

    public function voiceCall($msg, $type = '', $saveCode = false) {
        if (!env('SMS_REAL_SEND')) {
            $send = true;
        } else {
            $send = $this->voiceCallByApi($msg);
        }

        if($send) {
            if($saveCode) {
                DB::table('sms_codes')->insert([
                    'phone'      => $this->phone,
                    'code'       => Hash::make($saveCode),
                    'type'       => $type,
                    'created_at' => Carbon::now(),
                ]);
                //session([$type => $saveCode]);
            }
        }

        return $send;
    }


    public function callUser() {
        $send = $this->callByUser();
        return $send;
    }

    protected function preparePhoneList($phoneList) {
        $phoneArr = explode(',', $phoneList);
        foreach ($phoneArr as &$phone) {
            $phone = preg_replace("/[^0-9]+/", '', $phone);
            $phone = substr($phone, -10);
            $phone = ($this->addPlus ? '+' : '') . '7' . $phone;
        }
        return implode(',',$phoneArr);
    }


    public function checkCode($code, $type) {
        $lastCode = DB::table('sms_codes')
            ->whereRaw('phone like ?', sprintf('%%%s%%', $this->phone))
            ->where('type', $type)
            ->where('created_at', '>=', Carbon::parse('-1 days')->format('Y-m-d H:i:s'))
            ->orderBy('created_at', 'desc')
            ->first();
        if ($lastCode && Hash::check($code, $lastCode->code)) {
            return true;
        }
        return false;
    }


    public function deleteCodes($type) {
        DB::table('sms_codes')
            ->whereRaw('phone like ?', sprintf('%%%s%%', $this->phone))
            ->where('type', $type)
            ->where('created_at', '>=', Carbon::parse('-1 days')->format('Y-m-d H:i:s'))
            ->orderBy('created_at', 'desc')
            ->delete();
    }

    protected function getBlockSmsSessionKey($type = '') {
        return 'LAST_SEND_SMS_' . $type . '_' .$this->phone;
    }

    /**
     * Получаем время для разблокирвки отправки смс в секундах
     * @param string $type
     * @param int $max
     * @return int|mixed
     */
    public function getBlockSmsSeconds($type = '', $max = null)
    {
        if(is_null($max)) {
            $max = env('SMS_BLOCK_SECONDS', 120);
        }
        $max = intval($max);
        //$sessionKey = $this->getBlockSmsSessionKey($type);
        $lastCode = DB::table('sms_codes')
            ->whereRaw('phone like ?', sprintf('%%%s%%', $this->phone))
            ->where('type', $type)
            ->where('created_at', '>=', Carbon::parse('-1 days')->format('Y-m-d H:i:s'))
            ->orderBy('created_at', 'desc')
            ->first();
        if ($lastCode) {
            $lastCodeTimestamp = strtotime($lastCode->created_at);
            if((time() - $lastCodeTimestamp) < $max) {
                $left = $max - (time() - $lastCodeTimestamp);
                return $left;
            }
        }
        return 0;
    }

    /**
     * Проверяем есть ли уже код в сессии
     * @param string $type
     * @return bool
     */
    public function hasCodeInSession($type = '')
    {
        //$sessionKey = $this->getBlockSmsSessionKey($type);
        $lastCode = DB::table('sms_codes')
            ->whereRaw('phone like ?', sprintf('%%%s%%', $this->phone))
            ->where('type', $type)
            ->where('created_at', '>=', Carbon::parse('-1 days')->format('Y-m-d H:i:s'))
            ->orderBy('created_at', 'desc')
            ->first();
        if ($lastCode) {
            return true;
        }
        return false;
    }

    public function generateRandomPassword($length = 6)
    {
        if (!env('SMS_REAL_SEND')) {
            $password = str_repeat(2, $length);
        } else {
            $password = '';
            for ($i = 0; $i<$length; $i++)
            {
                $password .= mt_rand(0,9);
            }
        }
        return $password;
    }

    public function addVoiceMenu(VoiceMenu $menu)
    {
        $this->voiceMenu = $menu;
        return $this;
    }

    public function canUseVoiceCall(): bool
    {
        return false;
    }

    public function prepareVoiceMenu(): string
    {
        return '';
    }

    public function canUseEmailMessage(): bool
    {
        return false;
    }

    public function sendEmail($msg, $subject = '', $senderEmail = '', $senderName = ''): bool
    {
        return false;
    }

    public function canUseFlashCall(): bool
    {
        return false;
    }
}
