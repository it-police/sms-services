<?php

namespace ITPolice\SmsServices\Services;

use Illuminate\Support\Facades\Log;

class SMSCHelper extends SMSService implements \ITPolice\SmsServices\SMSService
{

    protected $addPlus = true;
    private $partnerId = 616254;

    public function sendInApi($msg)
    {
        $data = [
            'login'  => env('SMSC_LOGIN'),
            'psw'    => env('SMSC_PASSWORD'),
            'phones' => $this->phone,
            'mes'    => $msg,
            'fmt'    => 3,
            'cost'   => 3
        ];

        if ( ! empty($this->sender)) {
            $data['sender'] = $this->sender;
        }

        $result = $this->curlQuery($data);

        if (@$result->cnt) {
            return true;
        }

        /** В случае ошибки возвращаем вместе со статусом код ошибки */
        return [
            'status' => false,
            'error_code'   => $result->error_code ?? 10,
        ];
    }

    function curlQuery($data, $baseUrl = "https://smsc.ru/sys/send.php?")
    {
        $url = $baseUrl . http_build_query($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $body = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($body);

        if (env('SMSC_LOG_REQUESTS')) {
            Log::debug(__CLASS__, [
                'data'     => $data,
                'response' => $body
            ]);
        }
        return $json;
    }

    public function authCallByApi()
    {
        $data = [
            'login'  => env('SMSC_LOGIN'),
            'psw'    => env('SMSC_PASSWORD'),
            'phones' => $this->phone,
            'mes'    => 'code',
            'fmt'    => 3,
            'call'   => 1,
            'pp'     => $this->partnerId
        ];

        $result = $this->curlQuery($data);

        if ($result && ($result->code ?? false)) {
            return @$result->code;
        }

        return false;
    }

    public function voiceCallByApi($msg)
    {
        $data = [
            'login'  => env('SMSC_LOGIN'),
            'psw'    => env('SMSC_PASSWORD'),
            'voice'  => env('SMSC_VOICE', 'm2'),
            'phones' => $this->phone,
            'mes'    => $msg,
            'fmt'    => 3,
            'call'   => 1,
            'pp'     => $this->partnerId
        ];

        $result = $this->curlQuery($data);

        if (@$result->cnt) {
            return true;
        }

        return false;
    }

    public function callByUser()
    {
        $data = [
            'login'  => env('SMSC_LOGIN'),
            'psw'    => env('SMSC_PASSWORD'),
            'phone' => $this->phone,
            'fmt'    => 3,
        ];

        $result = $this->curlQuery($data,"https://smsc.ru/sys/wait_call.php?");

        if (isset($result->error) && $result->error) {
            Log::error(__CLASS__, [
                'data'     => $data,
                'response' => $result->error
            ]);
            return false;
        }
        if (isset($result->all_phones) && $result->all_phones) {
            return $result->all_phones;
        }

        return false;
    }

    public function canUseVoiceCall(): bool
    {
        return true;
    }

    public function prepareVoiceMenu(): string
    {
        if (!$this->voiceMenu)
            return '';

        $commands = collect($this->voiceMenu->getCommands());
        if ($commands->count() == 0)
            return '';

        $commandsAsString = collect();
        foreach ($commands as $command) {
            /**
             * @var VoiceMenuCommand $command
             */

            $prefix = '';
            if ($command->isSms()) {
                $prefix = 'sms:$phone,,';
            }
            $commandsAsString->push($command->getIndex() . ': ' . $prefix . $command->getText());
        }

        $menuText = str_replace("\n", ', ', $this->voiceMenu->getStartText());
        $endText = $this->voiceMenu->getEndText();
        $commands = $commandsAsString->implode("\n");

        return "\n{menu: $menuText\n$commands\n}\n\n$endText";
    }

    public function canUseEmailMessage(): bool
    {
        return true;
    }

    public function sendEmail($msg, $subject = '', $senderEmail = '', $senderName = ''): bool
    {
        $data = [
            'login'  => env('SMSC_LOGIN'),
            'psw'    => env('SMSC_PASSWORD'),
            'phones' => $this->email,
            'mes'    => $msg,
            'subj'   => $subject,
            'sender' => $senderEmail,
            'mail'   => 1,
            'pp'     => $this->partnerId
        ];

        $result = $this->curlQuery($data);

        if ($result && ($result->code ?? false)) {
            return @$result->code;
        }

        return false;
    }

    public function isActive(): bool
    {
        return ! empty(env('SMSC_LOGIN')) && ! empty(env('SMSC_PASSWORD'));
    }
}
