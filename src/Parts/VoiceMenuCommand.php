<?php

namespace ITPolice\SmsServices\Parts;

class VoiceMenuCommand
{
    const TYPE_SMS = 1;
    const TYPE_VOICE = 2;
    protected $index;
    protected $text;
    protected $type;

    public function addSmsCommand(int $index, string $text) {
        $this->index = $index;
        $this->text = $text;
        $this->type = self::TYPE_SMS;
    }

    public function addVoiceCommand(int $index, string $text) {
        $this->index = $index;
        $this->text = $text;
        $this->type = self::TYPE_VOICE;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function isSms(): bool
    {
        return $this->type == self::TYPE_SMS;
    }

    public function isVoice(): bool
    {
        return $this->type == self::TYPE_VOICE;
    }

}
