<?php

namespace ITPolice\SmsServices\Parts;

class VoiceMenu
{
    /**
     * @var string
     */
    protected $startText = '';
    /**
     * @var string
     */
    protected $endText = '';
    /**
     * @var VoiceMenuCommand[]
     */
    protected $commands = [];

    public function getStartText(): string
    {
        return $this->startText;
    }

    public function setStartText(string $startText): VoiceMenu
    {
        $this->startText = $startText;
        return $this;
    }

    public function getEndText(): string
    {
        return $this->endText;
    }

    public function setEndText(string $endText): VoiceMenu
    {
        $this->endText = $endText;
        return $this;
    }

    /**
     * @return VoiceMenuCommand[]
     */
    public function getCommands(): array
    {
        return $this->commands;
    }

    public function addCommand(VoiceMenuCommand $command): VoiceMenu
    {
        $this->commands[] = $command;
        return $this;
    }
}
