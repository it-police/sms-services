<?php


namespace ITPolice\SmsServices;

use ITPolice\SmsServices\Services\BeelineHelper;
use ITPolice\SmsServices\Services\DevHelper;
use ITPolice\SmsServices\Services\SMSCHelper;
use ITPolice\SmsServices\Services\SmskaSpbHelper;
use ITPolice\SmsServices\Services\SMSRUHelper;
use ITPolice\SmsServices\Services\QTSMSHelper;
use ITPolice\SmsServices\Services\StreamTelecomHelper;

final class SMSServiceFactory
{
    /**
     * @param $type
     * @return SMSService
     */
    public static function factory($type)
    {
        if ($type == 'sms.ru') {
            return new SMSRUHelper();
        } elseif ($type == 'smsc.ru') {
            return new SMSCHelper();
        } elseif ($type == 'beeline') {
            return new BeelineHelper();
        } elseif ($type == 'smska-spb.ru') {
            return new SmskaSpbHelper();
        } elseif ($type == 'qtelecom.ru') {
            return new QTSMSHelper();
        } elseif ($type == 'stream-telecom') {
            return new StreamTelecomHelper();
        } elseif ($type == 'dev') {
            return new DevHelper();
        }

        return null;
    }
}
